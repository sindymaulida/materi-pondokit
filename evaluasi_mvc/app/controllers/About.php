<?php

class About extends Controller {
    public function index($nama = 'kakasimwah', $pekerjaan = 'pelajar', $umur = 19)
    {
        if( !isset($_SESSION['nama']))
        {
            header("Location:" . BASEURL . "/home/index");
            exit;
        }
       $data['nama'] = $data;
       $data['pekerjaan'] = $pekerjaan;
       $data['umur'] = $umur;
       $data['judul'] = 'About ME';
       $this->view('templates/header', $data);
       $this->view('about/index', $data);
       $this->view('templates/footer');

    }
    public function page()
    {
        $data['judul'] = 'Pages';
        $this->view('templates/header', $data);
        $this->view('about/page');
        $this->view('templates/footer');
    }
}