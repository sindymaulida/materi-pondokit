<div class="container mt-3">

     <div class="row">
        <div class Flasher::flash(); ?>
        </div>
      </div> 


    <div class="row">
        <div class="col-6">
        <button type="button" class="btn btn-primary" data-toggle="modal" 
        data-target="#exampleModal">
            Tambah Data Mahasiswa
        </button>


        <div class="row mt-4">
        <div class="col-12">
          <form action="<?= BASEURL; ?>/Mahasiswa/cari" method="post">
          <div class="input-group mb-3">
            <input type="text" class="form-control" placeholder="cari Mahasiswa..." name="keyword" id="keyword" autocomplete="off">
            <div class="input-group-append">
              <button class="btn btn-primary" type="submit" id="tombolCari">Cari</button>
            </div>
        </div>
          </form>
      </div>
    </div> 
       
            <h3>Daftar Mahasiswa</h3>
                <ul class="list-group">
                    <?php foreach( $data['mhs'] as $mhs ) : ?>
                    <li class="list-group-item">
                        <?= $mhs['nama']; ?>
                        <a href="<?= BASEURL; ?>/Mahasiswa/hapus/<?= $mhs['id']; ?>" 
                        class="badge badge-danger float-right ml-1"
                          onclick="return confirm('yakin?');">Hapus</a>
                          <a href="<?= BASEURL; ?>/Mahasiswa/tampilData/<?= $mhs['id']; ?>" 
                        class="badge badge-success float-right">Ubah</a>
                        <a href="<?= BASEURL; ?>/Mahasiswa/detail/<?= $mhs['id']; ?>" 
                        class="badge badge-primary float-right ml-1">Detail</a>
                    </li>                    
                    <?php endforeach; ?> 
                </ul>
        </div>
    </div>
   

</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="judulModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="judulModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <form action="<?= BASEURL; ?>/Mahasiswa/tambah" method="post">
            <input type="hidden" nama="id" id="id">
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" id="nama" name="nama"  autocomplete="off" required>
            </div>

            <div class="form-group">
                <label for="nrp">NRP</label>
                <input type="number" class="form-control" id="nrp" name="nrp"> 
            </div>

            <div class="form-group">
              <label for="email">Email</label>
              <input type="email" class="form-control" id="email" name="email" placeholder="email@example.com">
            </div>

          <div class="form-group">
              <label for="jurusan">Jurusan</label>
              <select class="form-control" id="jurusan" name="jurusan">
                <option value="Programmer">Programmer</option>
                <option value="Teknik Komputer & Jaringan">Teknik Komputer & Jaringan</option>
                <option value="TKPI">TKPI</option>
                <option value="NKPI">NKPI</option>
                <option value="AGP">AGP</option>
                <option value="Teknik Lingkungan">Teknik Lingkungan</option>
            </select>

            <div class="form-group">
                <label for="alamat">Alamat</label>
                <input type="text" class="form-control" id="alamat" name="alamat">
            </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Tambah Data</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="btn btn-warning">
    <a href="<?= BASEURL;?>/home/logout">Logout</a>
</div>

