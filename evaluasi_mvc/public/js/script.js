$(function() {

    $('.tampilModalUbah').on('click', function(){
        
        // judulmodal untuk tambanya blum ada noh
        $('#judulModalLabel').html('Ubah Data Dosen');
        $('.modal-footer button[type=submit]').html('Ubah Data');
        $('.modal-body form').attr('action', 'http://localhost/materi-pondokit/Sprint-6/Tugas1/public/Dosen/Ubah');
        const id = $(this).data('id');
        

        $.ajax({
            url: 'http://localhost/materi-pondokit/Sprint-6/Tugas1/public/Dosen/getUbah/',
            data: {id :id},
            method: 'post',
            dataType: 'json',
            success: function(data) {
                 console.log(data);
                 $('#nama').val(data.nama);
                 $('#telp').val(data.telp);
                 $('#email').val(data.email);
                 if (data.mata_kuliah == 'Teknik Informatika'){
                     $('mata_kuliah').append('<option selectted value="Teknik Informatika">Teknik Informatika</option>')
                     $('mata_kuliah').append('<option value="Teknik Komputer & Jaringan">Teknik Komputer & Jaringan</option>')
                     $('mata_kuliah').append('<option value="Manajemen">Manajemen</option>')
                     $('mata_kuliah').append('<option value="Akutansi">Akutansi</option>')
                     $('mata_kuliah').append('<option value="TKPI">TKPI</option>')
                 }else if(data.mata_kuliah == 'Teknik Komputer & Jaringan'){
                    $('mata_kuliah').append('<option value="Teknik Informatika">TKPI</option>')
                    $('mata_kuliah').append('<option value="Teknik Komputer & Jaringan">Teknik Komputer & Jaringan</option>')
                    $('mata_kuliah').append('<option value="Manajemen">Manajemen</option>')
                    $('mata_kuliah').append('<option value="Akutansi">Akutansi</option>')
                    $('mata_kuliah').append('<option value="TKPI">TKPI</option>')
                 }else if(data.mata_kuliah == 'Manajemen'){
                    $('mata_kuliah').append('<option value="Teknik Informatika">TKPI</option>')
                    $('mata_kuliah').append('<option value="Teknik Komputer & Jaringan">Teknik Komputer & Jaringan</option>')
                    $('mata_kuliah').append('<option value="Manajemen">Manajemen</option>')
                    $('mata_kuliah').append('<option value="Akutansi">Akutansi</option>')
                    $('mata_kuliah').append('<option value="TKPI">TKPI</option>')
                 }else if(data.mata_kuliah == 'Akutansi'){
                    $('mata_kuliah').append('<option value="Teknik Informatika">TKPI</option>')
                    $('mata_kuliah').append('<option value="Teknik Komputer & Jaringan">Teknik Komputer & Jaringan</option>')
                    $('mata_kuliah').append('<option value="Manajemen">Manajemen</option>')
                    $('mata_kuliah').append('<option value="Akutansi">Akutansi</option>')
                    $('mata_kuliah').append('<option value="TKPI">TKPI</option>')
                 }else if(data.mata_kuliah == 'TKPI'){
                    $('mata_kuliah').append('<option value="Teknik Informatika">TKPI</option>')
                    $('mata_kuliah').append('<option value="Teknik Komputer & Jaringan">Teknik Komputer & Jaringan</option>')
                    $('mata_kuliah').append('<option value="Manajemen">Manajemen</option>')
                    $('mata_kuliah').append('<option value="Akutansi">Akutansi</option>')
                    $('mata_kuliah').append('<option value="TKPI">TKPI</option>')
                 }
                 $('#alamat').val(data.alamat);
                 $('#id').val(data.id);
            }
        });
    });

});