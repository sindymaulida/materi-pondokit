<!DOCTYPE html>
<html>
<head>
<title>PSR</title>
</head>
<body>
<h1>AUTOLOADER</h1>

<h2>Persegi Panjang</h2>
<form action="" method="post">
<table>
<tr><td>Panjang <input type="text" name="panjang"></td></tr>
<tr><td>Lebar <input type="text" name="lebar"></td></tr>
<tr><td><input type="submit" name="proses"></td></tr>
</table>
</form> 

<?php
use PSR\Bangun_Datar\Persegipanjang;
    spl_autoload_register(function($class){
        $class = explode ("\\", $class);
        $class = end($class);
        require_once '../PSR/Bangun_Datar/Persegipanjang.php';
    
    });

$Bangun_Datar = new Persegipanjang();
?>

<h2>Balok</h2>
<form action="" method="post">
<table>
<tr><td>Panjang <input type="text" name="panjang"></td></tr>
<tr><td>Lebar <input type="text" name="lebar"></td></tr>
<tr><td>Tinggi <input type="text" name="tinggi"></td></tr>
<tr><td><input type="submit" name="proses"></td></tr>
</table>
</form> 

<?php
use PSR\Bangun_Dimensi\Balok;
    spl_autoload_register(function($class){
        $class  = explode ("\\", $class);
        $class  = end($class);
        require_once '../PSR/Bangun_Dimensi/Balok.php';
    
    });

$Bangun_Dimensi = new Balok();
?>

