<?php

class Mahasiswa extends Controller {
    public function index()
    {
        if( !isset($_SESSION['nama']))
        {
            header("Location:" . BASEURL . "/home/index");
            exit;
        }

        $data['judul'] = 'Daftar Mahasiswa';
        $data['mhs'] = $this->model('Mahasiswa_model')->getAllMahasiswa();
        $this->view('templates/header', $data);
        $this->view('mahasiswa/index', $data);
        $this->view('templates/footer');
    }

    public function detail($id)
    {
        if( !isset($_SESSION['nama']))
        {
            header("Location:" . BASEURL . "/home/index");
            exit;
        }
        $data['judul'] = 'Detail Mahasiswa';
        $data['mhs'] = $this->model('Mahasiswa_model')->getMahasiswaById($id);
        $this->view('templates/header', $data);
        $this->view('mahasiswa/detail', $data);
        $this->view('templates/footer');
    }

    public function tambah()
    {
        if($this->model('Mahasiswa_model')->tambahDataMahasiswa($_POST) > 0) { // method tambahDataDosennnn mana
            Flasher::setFlash('berhasil', 'ditambahkan', 'success');
            header('Location: ' . BASEURL . '/Mahasiswa');
            exit;
        }else {
            Flasher::setFlash('gagal', 'ditambahkan', 'denger');
            header('Location: ' . BASEURL . '/Mahasiswa');
            exit;
        }
    }

     public function hapus($id)
     {           
         if($this->model('Mahasiswa_model')->hapusDataMahasiswa($id) > 0) {
            Flasher::setFlash('berhasil', 'dihapus', 'success');
            header('Location: ' . BASEURL . '/Mahasiswa');
            exit;
       }else {
            Flasher::setFlash('gagal', 'dihapus', 'denger');
           header('Location: ' . BASEURL . '/Mahasiswa');
            exit;
        }
    }

    //  public function getubah()
    // {
    //     echo json_encode($this->model('Mahasiswa_model')->getMahasiswaById($_POST['id']));
    // }

      public function tampilData($id)
    {
        if( !isset($_SESSION['nama']))
        {
            header("Location:" . BASEURL . "/home/index");
            exit;
        }
        $data['judul'] = 'Update mahasiswa';
        $data['mahasiswa'] = $this->model('Mahasiswa_model')->getmahasiswaById($id);
        // var_dump($data['mahasiswa']['id']);
        $this->view('templates/header', $data);
        $this->view('mahasiswa/update', $data);
        $this->view('templates/footer');
    }

     public function ubah()
    {
        
        if($this->model('Mahasiswa_model')->ubahDataMahasiswa($_POST) > 0) { 
            Flasher::setFlash('berhasil', 'diubah', 'success');
            header('Location: ' . BASEURL . '/Mahasiswa');
            exit;
        }else {
            Flasher::setFlash('gagal', 'diubah', 'denger');
            header('Location: ' . BASEURL . '/Mahasiswa');
        } 
    }

    public function cari()
    {
        if( !isset($_SESSION['nama']))
        {
            header("Location:" . BASEURL . "/home/index");
            exit;
        }
        $data['judul'] = 'Daftar Mahasiswa';
        $data['mhs'] = $this->model('Mahasiswa_model')->cariDataMahasiswa();
        $this->view('templates/header', $data);
        $this->view('mahasiswa/index', $data);
        $this->view('templates/footer');
    }

}


