<!DOCTYPE html>
<html>
<head>
<title>AUTOLOAD</title>
</head>
<body>
<h1>AUTOLOAD</h1>

<h2>Lingkaran</h2>
<form action="" method="post">
<table>
<tr><td>Jari - Jari <input type="text" name="jari"></td></tr>
<tr><td>Luas <input type="text" name="luas"></td></tr>
<tr><td><input type="submit" name=""></td></tr>
</table>
</form>

<?php

require_once __DIR__ . '/vendor/autoload.php';
use Bangun_Datar\UserLingkaran;

spl_autoload_register(function($class){
    $class = explode ("\\", $class);
    $class = end($class);

});

$Bangun1 = new UserLingkaran ();
?>

<h2>Persegi Panjang</h2>
<form action="" method="post">
<table>
<tr><td>Panjang <input type="text" name="panjang"></td></tr>
<tr><td>Lebar <input type="text" name="lebar"></td></tr>
<tr><td><input type="submit" name="proses"></td></tr>
</table>
</form>

<?php
require_once __DIR__ . '/vendor/autoload.php';
use MyApp\UserPersegipanjang;

spl_autoload_register(function($class){
    $class = explode ("\\", $class);
    $class = end($class);

});

$Bangun2 = new UserPersegipanjang ();
?>

<h2>Segitiga</h2>
<form action="" method="post">
<table>
<tr><td>Alas <input type="text" name="alas"></td></tr>
  <tr><td>Tinggi <input type="text" name="tinggi"></td></tr>
  <tr><td><input type="submit" name="proses"></td></tr>
</table>
</form>

<?php
require_once __DIR__ . '/vendor/autoload.php';
use src\UserSegitiga;

spl_autoload_register(function($class){
    $class = explode ("\\", $class);
    $class = end($class);

});

$Bangun3 = new UserSegitiga ();
?>