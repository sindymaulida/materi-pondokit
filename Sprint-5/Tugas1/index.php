<!DOCTYPE html>
<html>
<head>
<title>AUTOLOAD</title>
</head>
<body>
<h1>AUTOLOAD</h1>

<h2>Lingkaran</h2>
<form action="" method="post">
<table>
<tr><td>Jari - Jari <input type="text" name="jari"></td></tr>
<tr><td>Luas <input type="text" name="luas"></td></tr>
<tr><td><input type="submit" name=""></td></tr>
</table>
</form>
<?php
function autoloadBangunan1 ($Lingkaran)
{
$file = "Bangun_Datar/{$Lingkaran}.php";
if (is_readable($file)) {
require $file;
}
}

spl_autoload_register("autoloadBangunan1");

$lingkaran = new Lingkaran ();
$lingkaran->Bangunan1 ();

?>

<h2>Persegi Panjang</h2>
<form action="" method="post">
<table>
<tr><td>Panjang <input type="text" name="panjang"></td></tr>
<tr><td>Lebar <input type="text" name="lebar"></td></tr>
<tr><td><input type="submit" name="proses"></td></tr>
</table>
</form>  

<?php

function autoloadBangunan2 ($Persegipanjang)
{
$file = "Bangun_Datar/{$Persegipanjang}.php";
if (is_readable($file)) {
require $file;
}
}

spl_autoload_register("autoloadBangunan2");

$persegipanjang = new Persegipanjang();
$persegipanjang->Bangunan2();

?>


<h2>Segitiga</h2>
<form action="" method="post">
<table>
<tr><td>Alas <input type="text" name="alas"></td></tr>
  <tr><td>Tinggi <input type="text" name="tinggi"></td></tr>
  <tr><td><input type="submit" name="proses"></td></tr>
</table>
</form>

<?php
function autoloadBangunan3 ($Segitiga)
{

$file = "Bangun_Datar/{$Segitiga}.php";
if (is_readable($file)) {
require $file;
}
}

spl_autoload_register("autoloadBangunan3");

$segitiga = new Segitiga;
$segitiga->Bangunan3();

?>


</body>
</html>

