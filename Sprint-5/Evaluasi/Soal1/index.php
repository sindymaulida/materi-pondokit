<?php


function autoloadKota($class){
    $file = 'Kota/' . $class . '.php';
    if (is_readable($file)) {
        require $file;
    }
}

function autoloadTanaman($class){
    $file = 'Tanaman/' . $class . '.php';
    if (is_readable($file)) {
        require $file;
    }
}

spl_autoload_register('autoloadKota');
$jakarta = new Jakarta();
echo '<br>';
$padang = new Padang();

spl_autoload_register('autoloadTanaman');
$blog = new Blog();
echo '<br>';
$user = new User();