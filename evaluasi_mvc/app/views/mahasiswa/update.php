<div class="container mt-3">
    <h3><?=$data['judul']?></h3><br>
    <div class="card" style="width: 30rem;">
    <form action="<?= BASEURL; ?>/Mahasiswa/ubah/" method="POST">
            <input type="hidden" name="id" id="id" value="<?= $data['mahasiswa']['id']; ?>">
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" id="nama" name="nama" value="<?= $data['mahasiswa']['nama']; ?>">
            </div>

            <div class="form-group">
                <label for="nrp">NRP</label>
                <input type="number" class="form-control" id="nrp" name="nrp" value="<?= $data['mahasiswa']['nrp']; ?>"> 
            </div>

            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control" id="email" name="email" value="<?= $data['mahasiswa']['email']; ?>">
            </div>

            <div class="form-group">
                <label for="jurusa">Jurusan</label>
                <select class="form-control" id="jurusan" name="jurusan" value="<?= $data['mahasiswa']['jurusan']; ?>">
                  <option <?= ($data['mahasiswa']['jurusan'] == "Programmer") ? 'selected':""; ?> value="Programmer">Programmer</option>
                  <option <?= ($data['mahasiswa']['jurusan'] == "Teknik komputer & Jaringan") ? 'selected':""; ?> value="Teknik Komputer & Jaringan">Teknik Komputer & Jaringan</option>
                  <option <?= ($data['mahasiswa']['jurusan'] == "TKPI") ? 'selected':""; ?> value="TKPI">TKPI</option>
                  <option <?= ($data['mahasiswa']['jurusan'] == "NKPI") ? 'selected':""; ?> value="NKPI">NKPI</option>
                  <option <?= ($data['mahasiswa']['jurusan'] == "AGP") ? 'selected':""; ?> value="AGP">AGP</option>
                  <option <?= ($data['mahasiswa']['jurusan'] == "Teknik Lingkungan") ? 'selected':""; ?> value="Teknik Lingkungan">Teknik Lingkungan</option>
                </select>
            </div>

            <div class="form-group">
                <label for="alamat">Alamat</label>
                <input type="text" class="form-control" id="alamat" name="alamat" value="<?= $data['mahasiswa']['alamat']; ?>">
            </div>

            <button type="submit" class="btn btn-primary">Update Data</button>
        </form>
    </div>
</div>